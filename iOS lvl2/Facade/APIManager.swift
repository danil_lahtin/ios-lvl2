//
//  APIManager.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit


// Class is a facade singleton.
// Do work with data and internet
class APIManager: NSObject {
    
    // MARK: - Fields
    static let sharedInstance = APIManager()
    
    let dataManager = DataManager<Image>()
    let httpClient = HTTPClient()
    let imageCache = NSCache()
    
    // MARK: - DataManager work
    
    func getImage(atIndex index: Int) -> Image? {
        return self.dataManager.getItem(atIndex: index)
    }
    
    func getImages() -> [Image] {
        return self.dataManager.getItems()
    }
    
    func addImage(image: Image) {
        self.dataManager.addItem(image)
    }
    
    func addImage(image: Image, atIndex index: Int) {
        self.dataManager.addItem(image, atIndex: index)
    }
    
    func addImages(images: [Image]) {
        self.dataManager.addItems(images)
    }
    
    func deleteImage(atIndex index: Int) {
        self.dataManager.deleteItem(atIndex: index)
    }
    
    func deleteAll() {
        self.dataManager.deleteAll()
    }
    
    // MARK: - HTTPClient work
    
    func fetchImages(count: Int, completionHandler:(Bool) -> ()) {
        let request = self.httpClient.createURLRequest(params: ["a":"getRandom", "count":"\(count)"])
        self.httpClient.getJSON(request){(jsonData, error) in
            if error != nil {
                print(error?.localizedDescription)
                completionHandler(false)
                return
            }
            if let items = jsonData as? [String]{
                self.dataManager.deleteAll()
                for item in items {
                    let image = Image(withURLString: item)
                    self.dataManager.addItem(image)
                }
                completionHandler(true)
            } else {
                print("Wrong JSON")
                completionHandler(false)
                return
            }
            
        }
    }
    
    func downloadImage(fromURL urlString: String, completionHandler: (UIImage?, Bool) -> ()){
        if let cashedImage = self.imageCache.objectForKey(urlString) as? UIImage {
            completionHandler(cashedImage, true)
        }
        self.httpClient.getImage(withURL: urlString) {(image, success) in
            if success != nil && image != nil{
                self.imageCache.setObject(image!, forKey: urlString)
                completionHandler(image, success!)
            } else {
                completionHandler(image, false)
            }
        }
    }
    
    // MARK: - APIManager
    
    func cleanCache(){
        self.imageCache.removeAllObjects()
    }
}
