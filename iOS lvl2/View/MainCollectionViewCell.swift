//
//  MainCollectionViewCell.swift
//  iOS lvl2
//
//  Created by Danil Lahtin on 22/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    var imageURLString: String?
    
    var image: Image? {
        didSet{
            
            if let image = self.image {                self.imageURLString = image.getFullURLPath()
                APIManager.sharedInstance.downloadImage(fromURL: image.getFullURLPath(), completionHandler: { (imageObject, success) in
                    dispatch_async(dispatch_get_main_queue()){
                        if (image.getFullURLPath() == self.imageURLString) {
                            self.imageView.image = success ? imageObject : UIImage(named: "noImage")
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.hidden = true
                        }
                    }
                })
            }
            
        }
    }
    
    
    override func prepareForReuse() {
        self.image = nil
        self.imageView.image = nil
        self.activityIndicator.startAnimating()
        self.activityIndicator.hidden = false
        super.prepareForReuse()
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var textLabel: UILabel!
    
}
