//
//  AppDefaults.swift
//  iOS lvl2
//
//  Created by Danil Lahtin on 22/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import Foundation
import UIKit

struct AppDefaults {
    
    static let defaultURLString = "http://bnet.i-partner.ru/projects/calc/networkimages/"
    static let defaultTimeoutInterval = 10.0
    
    static let defaultNumberOfRowsInPortrait = CGFloat(2.0)
    static let defaultNumberOfRowsInLandscape = CGFloat(3.0)
    
    static let defaultMinimumLineSpacing = CGFloat(16.0)
    static let defaultMinimumInterItemSpacing = CGFloat(16.0)
    static let defaultLeftRightInset = CGFloat(32.0)
    static let defaultTopBottomInset = CGFloat(32.0)
    
    static let defaultImagesCount = 1000
}