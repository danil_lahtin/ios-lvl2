//
//  Image.swift
//  iOS lvl2
//
//  Created by Danil Lahtin on 22/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import Foundation

class Image: NSObject {
    
    var urlString = AppDefaults.defaultURLString
    
    override init() {
        super.init()
    }
    
    init(withURLString urlString: String){
        self.urlString = urlString
    }
    
    func getFullURLPath() -> String {
        var urlString = AppDefaults.defaultURLString
        urlString += self.urlString.substringFromIndex(self.urlString.startIndex.advancedBy(2))
        
        return urlString
    }
}