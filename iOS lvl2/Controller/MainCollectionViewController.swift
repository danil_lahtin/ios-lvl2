//
//  MainCollectionViewController.swift
//  iOS lvl2
//
//  Created by Danil Lahtin on 22/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ImageCell"

class MainCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var images = [Image]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APIManager.sharedInstance.fetchImages(AppDefaults.defaultImagesCount, completionHandler: { (success) in
            if success {
                self.images = APIManager.sharedInstance.getImages()
                dispatch_async(dispatch_get_main_queue()) {
                   // self.collectionView?.performBatchUpdates(nil, completion: nil)
                    self.collectionView?.reloadData()
                }
            }
        })
    }
    
    
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MainCollectionViewCell
    
        cell.image = self.images[indexPath.row]
        cell.textLabel.text = "\(indexPath.row)"
        return cell
    }
    
    

    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let orientation = UIApplication.sharedApplication().statusBarOrientation
        let itemsInRowCount = UIInterfaceOrientationIsLandscape(orientation) ? AppDefaults.defaultNumberOfRowsInLandscape : AppDefaults.defaultNumberOfRowsInPortrait
        
        let width = CGRectGetWidth(collectionView.frame)
        let insetsWidth = AppDefaults.defaultMinimumInterItemSpacing * CGFloat(itemsInRowCount - 1)
        let horizontalInset = AppDefaults.defaultLeftRightInset
        
        let size = (width - insetsWidth - horizontalInset * 2) / itemsInRowCount
        return CGSizeMake(size, size)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return AppDefaults.defaultMinimumLineSpacing
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return AppDefaults.defaultMinimumInterItemSpacing
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let verticalInset = AppDefaults.defaultTopBottomInset
        let horizontalInset = AppDefaults.defaultLeftRightInset
        let insets = UIEdgeInsets(top: verticalInset, left: horizontalInset, bottom: verticalInset, right: horizontalInset)
        return insets
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.collectionView?.performBatchUpdates(nil, completion: nil)
    }
    
    @IBAction func trashButtonTapped(sender: AnyObject) {
        APIManager.sharedInstance.cleanCache()
    }
    
    @IBAction func refreshButtonTapped(sender: AnyObject) {
        APIManager.sharedInstance.fetchImages(AppDefaults.defaultImagesCount) { (success) in
            if success {
                self.images = APIManager.sharedInstance.getImages()
                dispatch_async(dispatch_get_main_queue()) {
                    self.collectionView?.reloadData()
                }
            }
        }
    }

}
